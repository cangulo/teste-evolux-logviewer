'use strict';

/**
 * @ngdoc function
 * @name logviewerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the logviewerApp
 */
angular.module('logviewerApp')
  .controller('MainCtrl', function ($scope) {
    $scope.rows = [

    ];

    $scope.ip = '';

    $scope.listen = function() {

      // 192.168.33.11

      $scope.rows = [];

      $scope.status = '';

      var connString = "ws://" + $scope.ip + ":8080/";
      var ws;

      ws = new WebSocket(connString);

      ws.onopen = function(){
        $scope.status = 'Conexão OK - ' + connString;
	      $scope.$apply();
      };

      ws.onerror = function() {
        $scope.status = 'Falha na conexão - ' + connString;
        $scope.$apply();
      }

      ws.onmessage = function(message) {
        var msg = JSON.parse(message.data);

        for (var i in msg) {
          $scope.rows.push(msg[i]);
        }
        $scope.$apply();
        // $scope.rows.concat(msg.entries);
        // console.log($scope.rows);
        // $scope.$apply();
      };


    }


  });
