'use strict';

/**
 * @ngdoc overview
 * @name logviewerApp
 * @description
 * # logviewerApp
 *
 * Main module of the application.
 */
angular
  .module('logviewerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
